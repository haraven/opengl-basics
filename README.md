This is a basic C++ application that uses the OpenGL specification to display graphics.
All of the code defined here is free to use. Constructive criticism is welcome, just bear in mind that I am still an OpenGL newb.
Dependencies are not included. You will need:
- GLM
- GLFW
- GLEW

All of the above are public and available on the internet. If the need arises, I can provide links to the dependencies.
The tutorial I will be following is available at http://web.archive.org/web/20140627200840/http://www.arcsynthesis.org/gltut/index.html, or at https://paroj.github.io/gltut/.

You may contact me regarding this project, or game development in general, using the following:
Skype: haraven
Hotmail: corburadu@hotmail.com
G-mail: senekrum@gmail.com