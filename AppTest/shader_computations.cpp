#include "shader_computations.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <GL\glew.h>
#include "utils.h"

const GLfloat frag_loop_duration = 5.0f,
	vert_loop_duration = 5.0f;

namespace shader_data
{
    GLuint InitializeProgram(const GLchar* vertex_shader_path, const GLchar* fragment_shader_path)
    {
        std::vector<GLuint> shader_list = std::vector<GLuint>();

        LoadShader(shader_list, vertex_shader_path, GL_VERTEX_SHADER);
        LoadShader(shader_list, fragment_shader_path, GL_FRAGMENT_SHADER);

        GLuint program = CreateProgram(shader_list);
        std::for_each(shader_list.begin(), shader_list.end(), glDeleteShader);

        return program;
    }

    GLuint InitializeProgram(const std::vector<ShaderPathStruct>& shader_paths)
    {
        std::vector<GLuint> shader_list = std::vector<GLuint>();
        std::for_each(shader_paths.begin(), shader_paths.end(), [&shader_list](auto shader_file)
        {
            LoadShader(shader_list, shader_file.shader_path, shader_file.shader_type);
        });

        GLuint program = CreateProgram(shader_list);
		GLuint loop_duration_loc      = glGetUniformLocation(program, "loop_duration");
		GLuint screen_height_loc      = glGetUniformLocation(program, "screen_height");
		GLuint frag_loop_duration_loc = glGetUniformLocation(program, "frag_loop_duration");
		glUseProgram(program);
		glUniform1f(loop_duration_loc, vert_loop_duration); // initialize loop_duration
		glUniform1f(frag_loop_duration_loc, frag_loop_duration); // initialize frag_loop_duration
		int width, height;
		GetDesktopResolution(width, height);
		glUniform1f(screen_height_loc, static_cast<float>(height));
		glUseProgram(0);
        std::for_each(shader_list.begin(), shader_list.end(), glDeleteShader);
        return program;
    }

    GLuint InitializeProgram(const std::initializer_list<ShaderPathStruct>& shader_paths)
    {
        return InitializeProgram(std::vector<ShaderPathStruct>(shader_paths));
    }

    std::string GetShaderAsString(GLenum shader_type)
    {
        switch (shader_type)
        {
        case GL_VERTEX_SHADER:
            return "vertex";
        case GL_GEOMETRY_SHADER:
            return "geometry";
        case GL_FRAGMENT_SHADER:
            return "fragment";
        case GL_LINK_STATUS:
            return "linker";
        default:
            return "";
        }
    }

    std::string GetFileAsString(const std::string& file_path)
    {
        std::ifstream ifs(file_path);
        std::string content((std::istreambuf_iterator<char>(ifs)),
            (std::istreambuf_iterator<char>()));
        return content;
    }

    void LoadShader(std::vector<GLuint>& shader_list, const GLchar* shader_path, GLenum shader_type)
    {
        shader_list.push_back(CreateShader(shader_type, GetFileAsString(shader_path).c_str()));
    }

    void PrintError(GLenum shader_type, GLuint shader)
    {
        GLint info_log_len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_len);

        GLchar *info_log = new GLchar[info_log_len + 1];
        glGetShaderInfoLog(shader, info_log_len, NULL, info_log);

        std::cerr << GetShaderAsString(shader_type) << " error: " << info_log;
        delete[] info_log;
    }

    GLuint CreateShader(GLenum shader_type, const GLchar* shader_string)
    {
        GLuint shader = glCreateShader(shader_type);
        glShaderSource(shader, 1, &shader_string, NULL);
        glCompileShader(shader);
        GLint status;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
        if (status == GL_FALSE)
        {
            PrintError(shader_type, shader);
        }

        return shader;
    }

    GLuint CreateProgram(const std::vector<GLuint>& shader_list)
    {
        GLuint program = glCreateProgram();

        for (size_t iLoop = 0; iLoop < shader_list.size(); iLoop++)
            glAttachShader(program, shader_list[iLoop]);

        glLinkProgram(program);

        GLint status;
        glGetProgramiv(program, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            PrintError(GL_LINK_STATUS, program);
        }

        std::for_each(shader_list.begin(), shader_list.end(), [&program](const GLuint& shader)
        {
            glDetachShader(program, shader);
        });

        return program;
    }

    ShaderPathStruct::ShaderPathStruct(const GLchar* path, const GLenum& type)
    {
        shader_path = path;
        shader_type = type;
    }
}
