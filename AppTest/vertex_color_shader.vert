#version 330

layout (location = 0) in vec4 in_position;
layout (location = 1) in vec4 in_color;

// smooth is an interpolation qualifier -> TODO: read up on that.
smooth out vec4 color;

void main()
{
    gl_Position = in_position;
    color       = in_color;
}