#version 330

out vec4 output_color;

void main()
{
    float lerp_val = gl_FragCoord.y / 1080.0f;
    
    output_color = mix(vec4(0.25f, 0.71f, 1.0f, 1.0f),
        vec4(0.1f, 0.1f, 0.1f, 1.0f), lerp_val);
}