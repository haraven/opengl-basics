#version 330

layout(location = 0) in vec4 position;
uniform vec2 offset;

layout (location = 1) in vec4 in_color;

// smooth is an interpolation qualifier -> TODO: read up on that.
smooth out vec4 color;

void main()
{
    vec4 total_offset = vec4(offset.x, offset.y, 0.0, 0.0);
    gl_Position       = position + total_offset;
	color             = in_color;
}