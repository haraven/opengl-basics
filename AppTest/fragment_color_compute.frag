#version 330

out vec4 output;

uniform float frag_loop_duration;
uniform float time;
uniform float screen_height;

void main()
{
	const float pi = 3.14159f;
	const vec4 base_color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    float crt_time = mod(time, frag_loop_duration);
    float crt_lerp = crt_time / frag_loop_duration;
	float lerp_val = gl_FragCoord.y / screen_height;
	
	float custom_sine = sin(pi * crt_time / frag_loop_duration);
	vec4 crt_color = base_color - vec4(custom_sine, 0.0f, custom_sine, 0.0f);

	output = mix(crt_color, vec4(0.1f, 0.1f, 0.1f, 1.0f), lerp_val);
}