#define _USE_MATH_DEFINES
#include "position_computations.h"

#include <iostream>
#include <algorithm>
#include <chrono>
#include <thread>
#include <cmath>
#include <vector>
#include "utils.h"

void pos_offsets::ComputePosOffsets(float& x, float& y)
{
	const float loop_duration = 5.0f,
		scale = static_cast<float>(M_PI) * 2.0f / loop_duration;
	float elapsed_time = static_cast<float>(glfwGetTime());
	float crt_time_through_loop = fmodf(elapsed_time, loop_duration);

	x = cosf(crt_time_through_loop * scale) * 0.5f;
	y = sinf(crt_time_through_loop * scale) * 0.5f;
}

void pos_offsets::AdjustVertexData(const float& x, const float& y, float vertex_positions[], const int& array_count, GLuint& position_buffer)
{
	std::vector<float> new_data(array_count);
	memcpy(&new_data[0], vertex_positions, array_count * sizeof(float));

	for (int i = 0; i < array_count; i += 4)
	{
		new_data[i] += x;
		new_data[i + 1] += y;
	}

	glBindBuffer(GL_ARRAY_BUFFER, position_buffer);
	glBufferSubData(GL_ARRAY_BUFFER, 0, array_count * sizeof(float), &new_data[0]);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
