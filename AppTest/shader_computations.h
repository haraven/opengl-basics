#pragma once
#include <string>
#include <vector>
#include <GL\glew.h>

namespace shader_data
{
    struct ShaderPathStruct
    {
        ShaderPathStruct(const GLchar* path, const GLenum& type);
        
        const GLchar* shader_path;
        GLenum shader_type;
    };

    void LoadShader(std::vector<GLuint>& shader_list, const GLchar* shader_path, GLenum shader_type);

    GLuint InitializeProgram(const GLchar* vertex_shader_path, const GLchar* fragment_shader_path);
    GLuint InitializeProgram(const std::vector<ShaderPathStruct>& shader_paths);
    GLuint InitializeProgram(const std::initializer_list<ShaderPathStruct>& shader_paths);

    GLuint CreateShader(GLenum shader_type, const GLchar* shader_path);

    GLuint CreateProgram(const std::vector<GLuint>& shader_list);

    std::string GetShaderAsString(GLenum shader_type);

    void PrintError(GLenum shader_type, GLuint shader);
}