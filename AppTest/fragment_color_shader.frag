#version 330

smooth in vec4 color;

out vec4 out_color;

void main()
{
    out_color = color;
}