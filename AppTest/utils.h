#pragma once

#include <utility>

enum RetCodes
{
	SUCCESS = 0,
	ERR_GLFW_INIT,
	ERR_GLEW_INIT,
	ERR_WND_CREATE
};

// Windows-only: changes the value of the first argument to the window width, and that of the second argument to the window height
void GetDesktopResolution(int& width, int& height);
std::pair<unsigned long, unsigned long> GetDesktopResolution();