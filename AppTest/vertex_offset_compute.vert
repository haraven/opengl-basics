#version 330

layout(location = 0) in vec4 position;
uniform float loop_duration;
uniform float time;

layout (location = 1) in vec4 in_color;

smooth out vec4 color;

void main()
{
	const float pi = 3.14159f;
    float scale    = pi * 2.0f / loop_duration;
    
    float crt_time    = mod(time, loop_duration);
    vec4 total_offset = vec4
	(
        cos(crt_time * scale) * 0.5f,
        sin(crt_time * scale) * 0.5f,
        0.0f,
        0.0f
	);
    
    gl_Position = position + total_offset;
	color       = in_color;
}