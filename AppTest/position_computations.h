#pragma once

#include <GL\glew.h>
#include <GLFW\glfw3.h>

namespace pos_offsets
{
	/**
	* From the ArcSynthesis OpenGL tut:
	* This function computes offsets in a loop.
	* The offsets produce circular motion, and the offsets will reach the beginning of the circle every 5 seconds (controlled by loop_duration).
	* The function glfwGetTime() retrieves the integer time in milliseconds since GLFWInit was called.
	* The fmodf function computes the floating-point modulus of the time. In lay terms, it takes the first parameter and returns the remainder of the division between that and the second parameter.
	* Thus, it returns a value on the range [0, loop_duration), which is what we need to create a periodically repeating pattern.

	*The cosf and sinf functions compute the cosine and sine respectively. It is not important to know exactly how these functions work, but they effectively compute a circle of diameter 2.
	* By multiplying by 0.5f, it shrinks the circle down to a circle with a diameter of 1.
	*/
	void ComputePosOffsets(float& x, float& y);
	
	/**
	* From the ArcSynthesis OpenGL tut:
	* This function works by copying the vertex data into a std::vector, then applying the offset to the X and Y coordinates of each vertex. The last three lines are the OpenGL-relevant parts.
	* First, the buffer object containing the positions is bound to the context. Then the new function glBufferSubData is called to transfer this data to the buffer object.

	* The difference between glBufferData and glBufferSubData is that the SubData function does not allocate memory.
	* glBufferData specifically allocates memory of a certain size; glBufferSubData only transfers data to the already existing memory.
	* Calling glBufferData on a buffer object that has already been allocated tells OpenGL to reallocate this memory, throwing away the previous data and allocating a fresh block of memory.
	* Whereas calling glBufferSubData on a buffer object that has not yet had memory allocated by glBufferData is an error.
	* Think of glBufferData as a combination of malloc and memcpy, while glBufferSubData is just memcpy.
	* The glBufferSubData function can update only a portion of the buffer object's memory.
	* The second parameter of the function is the byte offset into the buffer object to begin copying at, and the third parameter is the number of bytes to copy.
	* The fourth parameter is our array of bytes to be copied into the location of the buffer object.

	* The last line of the function is simply unbinding the buffer object. It is not strictly necessary, but it is good form to clean up binds after making them.
	
	* ---
	* Personal note
	* ---
	* I had to add an array_count parameter, because of 2 reasons:
	* 1. the compiler doesn't correctly compute the number of elements, for an array that's given as argument to a function (float[] becomes float*, which messes up sizeof()).
	* 2. my vertex buffer data array also contains color information, so I have to specify how many elements are used in pos. computation, otherwise this function will change the color values as well.
	*/
	void AdjustVertexData(const float& x, const float& y, float vertex_positions[], const int& array_count, GLuint& position_buffer);
}