#include "utils.h"

#include <wtypes.h>

void GetDesktopResolution(int & width, int & height)
{
	RECT wnd;

	const HWND hdesktop = GetDesktopWindow();
	GetWindowRect(hdesktop, &wnd);

	width = wnd.right;
	height = wnd.bottom;
}

std::pair<unsigned long, unsigned long> GetDesktopResolution()
{
	int width, height;
	GetDesktopResolution(width, height);
	return std::pair<unsigned long, unsigned long>(width, height);
}
