#include <iostream>
#include "utils.h"
#include "shader_computations.h"
#include "position_computations.h"
#include "application_setup.h"

GLuint program, vertex_buffer;
GLFWwindow* wnd;
GLfloat vertex_buffer_data[] =
{
	0.0f,   0.5f,   0.0f, 1.0f,  // left vertex
	0.5f,  -0.366f, 0.0f, 1.0f,  // right vertex
	-0.5f,  -0.366f, 0.0f, 1.0f, // top vertex
	1.0f,   0.0f,   0.0f, 1.0f,  // left vertex color - RED
	0.0f,   1.0f,   0.0f, 1.0f,  // right vertex color - GREEN
	0.0f,   0.0f,   1.0f, 1.0f,  // top vertex color - BLUE
};

// handles any errors thrown by GLFW
void ErrCallbackFn(const int errcode, const char* msg);
// handles key input from a GLFWwindow
void KeyCallback(GLFWwindow* wnd, GLint key, GLint scan_code, GLint action, GLint modifiers);

// generates a single vertex array, and binds a single variable to it
GLuint SetupVertexArray(GLuint& arr);
// generates a single vertex buffer, and binds a single variable to it
GLuint SetupVertexBuffer(GLuint& arr);

// draws triangles having 4 coords, from the vertex_buffer_id, in the given progr_id
void PrepDisplayBuffer(GLuint progr_id, GLuint vertex_buffer_id);

// initializes a default window to be displayed on the screen.
RetCodes init_window();

int main()
{
	RetCodes res = init_window();
	if (res != SUCCESS)
		return res;

    // setup the vertex buffer
    vertex_buffer = SetupVertexBuffer(vertex_buffer);
    
    // initialize the program
    shader_data::ShaderPathStruct fragment_shader = shader_data::ShaderPathStruct("fragment_color_compute.frag", GL_FRAGMENT_SHADER);
    shader_data::ShaderPathStruct vertex_shader = shader_data::ShaderPathStruct("vertex_offset_compute.vert", GL_VERTEX_SHADER);
    program = shader_data::InitializeProgram({ vertex_shader, fragment_shader });

    // main program loop. Constantly clear the screen and re-draw the graphical element(s), then swap the buffers so that the new graphical elements are displayed. Loop breaks when escape is pressed (event handled by KeyCallback()).
	do
	{
        PrepDisplayBuffer(program, vertex_buffer);

		glfwSwapBuffers(wnd);
		glfwPollEvents();
	} while(!glfwWindowShouldClose(wnd));

	glfwTerminate();
	return SUCCESS;
}

void ErrCallbackFn(const int errcode, const char* msg)
{
    std::cerr << "Error " << errcode << ": " << msg << std::endl;
}

void KeyCallback(GLFWwindow * wnd, GLint key, GLint scan_code, GLint action, GLint modifiers)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwTerminate();
        exit(SUCCESS);
    }
}

void PrepDisplayBuffer(GLuint progr_id, GLuint vertex_buffer_id)
{
	//float x = 0.0f, y = 0.0f;
	//pos_offsets::ComputePosOffsets(x, y);
	//pos_offsets::AdjustVertexData(x, y, vertex_buffer_data, sizeof(vertex_buffer_data) / (sizeof(float) * 2), vertex_buffer_id);

    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(progr_id);

	GLuint time_loc = glGetUniformLocation(progr_id, "time");
	glUniform1f(time_loc, static_cast<GLfloat>(glfwGetTime()));

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer
    (
        NULL,
        4,
        GL_FLOAT,
        GL_FALSE,
        0,
        nullptr // buffer offset
    );
    glVertexAttribPointer
    (
        1,
        4,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void *)48 // buffer offset
    );

	// draw first triangle
    glDrawArrays(GL_TRIANGLES, 0, 3);
	// prepare & draw a second triangle
	glUniform1f(time_loc, static_cast<GLfloat>(glfwGetTime()) + 2.5f);
	glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glUseProgram(0);
}

RetCodes init_window()
{
	// set a new error callback function, to be called whenever a GLFW error occurs
	glfwSetErrorCallback(ErrCallbackFn);

	GLint result = glfwInit();
	if (result != GL_TRUE)
	{
		std::cerr << "An error occurred while initializing GLFW. Error code: " << result << ". The application will now close." << std::endl;
		return ERR_GLFW_INIT;
	}

	std::pair<GLulong, GLulong> screen_resolution = GetDesktopResolution();

	// create a full-screen window on the primary monitor:
	// 4x AA, OpenGL version 3.3, forward-compatible for Mac OS, modern OpenGL.
	wnd = SetupMainWindow(16, 3, 3, GL_TRUE, GLFW_OPENGL_CORE_PROFILE, screen_resolution.first, screen_resolution.second);
	if (wnd == nullptr)
	{
		return ERR_WND_CREATE;
	}

	// set newly created window as current context, and add an event handler for the keypresses
	glfwMakeContextCurrent(wnd);
	glfwSetInputMode(wnd, GLFW_STICKY_KEYS, GL_TRUE); // make keyboard input be captured by window
	glfwSetKeyCallback(wnd, KeyCallback);

	// setup GLEW
	glewExperimental = true;
	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		ErrCallbackFn(err, "Error initializing GLEW.");
		return ERR_GLEW_INIT;
	}

	return SUCCESS;
}

GLuint SetupVertexArray(GLuint& arr)
{
    glGenVertexArrays(1, &arr);
    glBindVertexArray(arr);

    return arr;
}

GLuint SetupVertexBuffer(GLuint& arr)
{    // setup a single vertex array
	GLuint vertex_array;
	vertex_array = SetupVertexArray(vertex_array);

    glGenBuffers(1, &arr);
    glBindBuffer(GL_ARRAY_BUFFER, arr);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
    return arr;
}