#include "application_setup.h"

GLFWwindow* SetupMainWindow(GLint sample_count, GLint openGL_version_major, GLint openGL_version_minor, GLint is_forward_compatible, GLint openGL_profile, GLulong width, GLulong height)
{
	glfwWindowHint(GLFW_SAMPLES, sample_count);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, openGL_version_major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, openGL_version_minor);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, is_forward_compatible);
	glfwWindowHint(GLFW_OPENGL_PROFILE, openGL_profile);

	return glfwCreateWindow(width, height, "OpenGL application", glfwGetPrimaryMonitor(), nullptr);
}