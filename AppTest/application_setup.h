#pragma once

#include <GL\glew.h>
#include <GLFW\glfw3.h>

// sets up a new window, using the specified parameters as window settings
GLFWwindow* SetupMainWindow(GLint sample_count, GLint openGL_version_major, GLint openGL_version_minor, GLint is_forward_compatible, GLint openGL_profile, GLulong width, GLulong height);